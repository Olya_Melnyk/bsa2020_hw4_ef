﻿using System;
using HW3.DAL.Repositories;
using HW3.DAL.Models;
using System.Collections.Generic;
using System.Linq;
using HW3.BLL.ServicesAbstraction;
using AutoMapper;
using HW3.BLL.DTO;
using HW3.DAL.Abstracts;

namespace HW3.BLL.Services
{
    public class TeamService:ITeamService
    {
        private readonly IRepository<Team> _repository;
        private readonly IRepository<User> _repositoryUser;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public TeamService(IUnitOfWork unitOfWork,IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _repository =_unitOfWork.GetRepository<Team>();
            _repositoryUser = _unitOfWork.GetRepository<User>();
            _mapper = mapper; 
        }

        public TeamDTO GetTeamById(int id)
        {
            if (_repository.Get(id) == null) throw new Exception($"Not found team with id={id}");
            else return _mapper.Map<TeamDTO>(_repository.Get(id));
        }

        public List<TeamDTO> GetTeams()
        {
            return _mapper.Map<List<TeamDTO>>(_repository.Get());
        }

        public void DeleteTeam(int id)
        {
            if (_repository.Get(id) == null)
                throw new Exception($"Not found team with id={id}");
            else
            {
                DeleteUsersByTeamId(id);
                _repository.Delete(id);
            }
        }

        public void DeleteTeam(TeamDTO team)
        {
            if (_repository.Get(team.Id) == null) throw new Exception($"Not found team with id={team.Id}");
            else
            {
                DeleteUsersByTeamId(team.Id);
                _repository.Delete(_mapper.Map<Team>(team)); 
            }
        }

        public void DeleteUsersByTeamId(int id)
        {
            var users = _repositoryUser.Get().Where(x => x.TeamId == id).ToList();
            for (int i = 0; i < users?.Count(); i++)
            {
                _repositoryUser.Get(users[i].Id).TeamId = null;
            }
        }
        public void UpdateTeam(TeamDTO team)
        {
            if (_repository.Get(team.Id) == null) throw new Exception($"Not found team with id={team.Id}");
            _repository.Update(_mapper.Map<Team>(team));
        }

        public void CreateTeam(TeamDTO team)
        {
            if (team == null) throw new Exception("You can`t create empty team");
            else _repository.Create(_mapper.Map<Team>(team));
        }
        //Task 4
        public List<TeamPlayersDTO> GetListUserByTeamAndMoreThenTenYearsOld()
        {
            return _repository.Get()
                .GroupJoin(
                _repositoryUser.Get(),
                t => t.Id,
                u => u.TeamId,
                (team, userList) =>
                new TeamPlayersDTO()
                {
                    Id = team.Id,
                    Name = team.Name,
                    ListUser = _mapper.Map<List<UserDTO>>(userList.Where(user => user.Birthday.Year < 2010)
                    .OrderByDescending(u=>u.RegisteredAt)
                    .ToList())
                }).ToList();
        }
    }
}
