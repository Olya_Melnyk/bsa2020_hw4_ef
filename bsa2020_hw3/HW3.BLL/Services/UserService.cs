﻿using System;
using HW3.DAL.Repositories;
using HW3.DAL.Models;
using System.Collections.Generic;
using System.Linq;
using HW3.BLL.ServicesAbstraction;
using AutoMapper;
using HW3.BLL.DTO;
using HW3.DAL.Abstracts;

namespace HW3.BLL.Services
{
    public class UserService:IUserService
    {
        private readonly IRepository<User> _repository;
        private readonly IRepository<TaskModel> _repositoryTask; 
        private readonly IRepository<Project> _repositoryProject;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public UserService(IUnitOfWork unitOfWork,IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _repository = _unitOfWork.GetRepository<User>();
            _repositoryTask = _unitOfWork.GetRepository<TaskModel>();
            _repositoryProject = _unitOfWork.GetRepository<Project>();
            _mapper = mapper;
        }

        public void DeleteUser(int id)
        {
            if (_repository.Get(id) == null) throw new Exception($"Not found user with id={id}");
            else
            {
                DeleteDataForUser(id);
                _repository.Delete(id);
            }
        }

        public void DeleteUser(UserDTO user)
        {
            if (user == null) throw new Exception("You can`t create empty user");
            else
            {
                DeleteDataForUser(user.Id);
                _repository.Create(_mapper.Map<User>(user));
            }
        }

        public void DeleteDataForUser(int id)
        {
            var tasks = _repositoryTask.Get().Where(x => x.PerformerId == id).ToList();
            for (int i = 0; i < tasks?.Count(); i++)
            {
                _repositoryTask.Get(tasks[i].Id).PerformerId = null;
            }
            var projects = _repositoryProject.Get().Where(x => x.AuthorId == id).ToList();
            for (int i = 0; i < projects?.Count(); i++)
            {
                _repositoryProject.Delete(projects[i].Id);
            }
        }
        public void UpdateUser(UserDTO user)
        {
            if (_repository.Get(user.Id) == null) throw new Exception($"Not found user with id={user.Id}");
            _repository.Update(_mapper.Map<User>(user));
        }

        public void CreateUser(UserDTO user)
        {
            if (user == null) throw new Exception($"Can`t create empty user");
            else _repository.Create(_mapper.Map<User>(user));
        }

        public List<UserDTO> GetUsers()
        {
            return _mapper.Map<List<UserDTO>>(_repository.Get());
        }

        public UserDTO GetUserById(int id)
        {
            return _mapper.Map<UserDTO>(_repository.Get(id));
        }
        //Task 5
        public List<UserDTO> GetListUserByFirstName()
        {
            return _repository.Get().GroupJoin(
                _repositoryTask.Get(),
                u => u.Id,
                t => t.PerformerId,
                (u, t) => {
                    u.Tasks = t.OrderByDescending(x => x.Name.Length).ToList();
                    return u;
                })
                .OrderBy(x => x.FirstName)
                .Select(u=>_mapper.Map<UserDTO>(u)).ToList();
        }
        //Task 6
        public AboutLastProjectDTO GetInfoAboutLastProjectByUserId(int authorId)
        {
            User user = _repository.Get(authorId);
            List<Project> projectList = _repositoryProject.Get();
            List<TaskModel> taskList = _repositoryTask.Get();
            AboutLastProject project = projectList
                .GroupJoin(
                taskList,
                t => t.Id,
                p => p.Id,
                (t, p) => new AboutLastProject()
                {
                    User = user,
                    LastProject = projectList
                    .Where(x => x.AuthorId == authorId)
                    .OrderByDescending(x => x.CreatedAt)
                    .FirstOrDefault(),
                    CountTasks = taskList
                    .Where(task => task.ProjectId == projectList
                    .Where(x => x.AuthorId == authorId)
                    .OrderByDescending(x => x.CreatedAt)
                    .First().Id
                    ).Count(),
                    CountNotFinishedOrCanceledTasks = taskList
                    .Where(task => ((int)task.State != 2)
                    && task.PerformerId == authorId)
                    .Count(),
                    LongestTask = taskList
                    .Where(task => task.PerformerId == authorId)
                    .OrderByDescending(x => x.FinishedAt - x.CreatedAt)
                    .FirstOrDefault()
                })
                .FirstOrDefault();
            return _mapper.Map<AboutLastProjectDTO>(project);
        }
    }
}
