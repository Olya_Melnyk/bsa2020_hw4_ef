﻿using HW3.BLL.DTO;
using HW3.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace HW3.BLL.ServicesAbstraction
{
    public interface ITaskService
    {
        void DeleteTask(int id);
        void DeleteTask(TaskDTO task);
        void UpdateTask(TaskDTO task);
        void CreateTask(TaskDTO task);
        List<TaskDTO> GetTasks();
        TaskDTO GetTaskById(int id);
        Dictionary<int, string> GetListFinishedTasksAt2020(int performerId);
        List<TaskDTO> GetTasksForUser(int performerId);
    }
}
