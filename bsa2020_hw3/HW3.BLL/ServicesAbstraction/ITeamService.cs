﻿using HW3.BLL.DTO;
using HW3.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace HW3.BLL.ServicesAbstraction
{
    public interface ITeamService
    {
        void DeleteTeam(int id);
        void DeleteTeam(TeamDTO team);
        void UpdateTeam(TeamDTO team);
        void CreateTeam(TeamDTO team);
        List<TeamDTO> GetTeams();
        TeamDTO GetTeamById(int id);
        List<TeamPlayersDTO> GetListUserByTeamAndMoreThenTenYearsOld();

    }
}
