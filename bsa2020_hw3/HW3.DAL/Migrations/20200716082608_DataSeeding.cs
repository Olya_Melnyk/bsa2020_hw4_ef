﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HW3.DAL.Migrations
{
    public partial class DataSeeding : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "TeamList",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[] { 3, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "asap" });

            migrationBuilder.InsertData(
                table: "TeamList",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[] { 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "psy" });

            migrationBuilder.InsertData(
                table: "UserList",
                columns: new[] { "Id", "Birthday", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Dekster", "Hanting", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null });

            migrationBuilder.InsertData(
                table: "UserList",
                columns: new[] { "Id", "Birthday", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Charly", "Bob", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1 });

            migrationBuilder.InsertData(
                table: "ProjectList",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 1, 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Arrr", "Arrrrrr", 1 });

            migrationBuilder.InsertData(
                table: "TaskList",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Add seeding data", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Task3", 2, 1, 0 });

            migrationBuilder.InsertData(
                table: "TaskList",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Add validation on models", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Task3", 1, 1, 0 });

            migrationBuilder.CreateIndex(
                name: "IX_ProjectList_AuthorId",
                table: "ProjectList",
                column: "AuthorId");

            migrationBuilder.AddForeignKey(
                name: "FK_ProjectList_UserList_AuthorId",
                table: "ProjectList",
                column: "AuthorId",
                principalTable: "UserList",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProjectList_UserList_AuthorId",
                table: "ProjectList");

            migrationBuilder.DropIndex(
                name: "IX_ProjectList_AuthorId",
                table: "ProjectList");

            migrationBuilder.DeleteData(
                table: "TaskList",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "TaskList",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "TeamList",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "ProjectList",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "UserList",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "UserList",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "TeamList",
                keyColumn: "Id",
                keyValue: 1);
        }
    }
}
