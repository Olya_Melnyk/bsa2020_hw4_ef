﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HW3.DAL.Migrations
{
    public partial class AddAboutLastProjectTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AboutLastProject",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(nullable: false),
                    ProjectId = table.Column<int>(nullable: false),
                    CountTasks = table.Column<int>(nullable: false),
                    CountNotFinishedOrCanceledTasks = table.Column<int>(nullable: false),
                    LongestTaskId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AboutLastProject", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AboutLastProject_TaskList_LongestTaskId",
                        column: x => x.LongestTaskId,
                        principalTable: "TaskList",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AboutLastProject_ProjectList_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "ProjectList",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AboutLastProject_UserList_UserId",
                        column: x => x.UserId,
                        principalTable: "UserList",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AboutLastProject_LongestTaskId",
                table: "AboutLastProject",
                column: "LongestTaskId");

            migrationBuilder.CreateIndex(
                name: "IX_AboutLastProject_ProjectId",
                table: "AboutLastProject",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_AboutLastProject_UserId",
                table: "AboutLastProject",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AboutLastProject");
        }
    }
}
