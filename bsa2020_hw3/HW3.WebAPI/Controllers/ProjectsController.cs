﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HW3.BLL.DTO;
using HW3.BLL.ServicesAbstraction;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HW3.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectService _projectService;
        public ProjectsController(IProjectService projectService)
        {
            _projectService = projectService;
        }
        [HttpGet]
        public ActionResult<List<ProjectDTO>> GetAllTasks()
        {
            try
            {
                return new JsonResult(_projectService.GetProjects());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                return new JsonResult(_projectService.GetProjectById(id));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpPost]
        public IActionResult Post([FromBody] ProjectDTO project)
        {
            try
            {
                _projectService.CreateProject(project);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }
        [HttpPut]
        public IActionResult UpdateProject([FromBody] ProjectDTO project)
        {
            try
            {
                _projectService.UpdateProject(project);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpDelete("{id:int}")]
        public IActionResult DeleteProject(int id)
        {
            try
            {
                _projectService.DeleteProject(id);
                return NoContent();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet("tasks")]
        public IActionResult GetInfoAboutProjects()
        {
            try
            {
                var a = _projectService.GetInfoAboutProjects();
                return new JsonResult(a);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet("counttasks/{id:int}")]//task 1
        public IActionResult GetCountTasksByUser(int id)
        {
            try
            {
                return new JsonResult(_projectService.GetCountTasksByUser(id).ToList());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
