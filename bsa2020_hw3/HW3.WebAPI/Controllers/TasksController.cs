﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HW3.BLL.DTO;
using HW3.BLL.ServicesAbstraction;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HW3.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly ITaskService _taskService;
        public TasksController(ITaskService taskService)
        {
            _taskService = taskService;
        }
        [HttpGet]
        public ActionResult<List<TaskDTO>> GetAllTasks()
        {
            try
            {
                return new JsonResult(_taskService.GetTasks());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                return new JsonResult(_taskService.GetTaskById(id));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpPost]
        public IActionResult Post([FromBody]TaskDTO task)
        {
            try
            {
                _taskService.CreateTask(task);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpPut]
        public IActionResult UpdateTask([FromBody] TaskDTO task)
        {
            try
            {
                _taskService.UpdateTask(task);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpDelete("{id:int}")]
        public IActionResult DeleteTask(int id)
        {
            try
            {
                _taskService.DeleteTask(id);
                return NoContent();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet("forUser/{id:int}")]//task 2
        public IActionResult GetTasksForUser(int id)
        {
            try
            {
                return new JsonResult(_taskService.GetTasksForUser(id));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet("finished2020/{id:int}")]//task 3
        public IActionResult GetListFinishedTasksAt2020(int id)
        {
            try
            {
                return new JsonResult(_taskService.GetListFinishedTasksAt2020(id).ToList());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


    }
}
